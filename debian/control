Source: sre-yield
Section: python
Priority: optional
Maintainer: Debian Fonts Task Force <debian-fonts@lists.debian.org>
Uploaders: Paulo Roberto Alves de Oliveira (aka kretcheu) <kretcheu@gmail.com>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               pybuild-plugin-pyproject,
               python3-setuptools,
               python3-all
Standards-Version: 4.6.0.1
Rules-Requires-Root: no
Homepage: https://github.com/google/sre_yield/
Vcs-Browser: https://salsa.debian.org/fonts-team/sre-yield
Vcs-Git: https://salsa.debian.org/fonts-team/sre-yield.git

Package: python3-sre-yield
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}
Description: Expands a regular expression to its possible matches
 The goal of sre_yield is to efficiently generate all values that can match a
 given regular expression, or count possible matches efficiently. It uses the
 parsed regular expression, so you get a much more accurate result than trying
 to just split strings.
